# standard lib
from pathlib import Path

import os


# 3rd party
from dotenv import load_dotenv

# 1st party
from .base import *  # noqa: F401,F403

# load .env file
DOTENV = Path(BASE_DIR) / ".env"
load_dotenv(dotenv_path=DOTENV, override=False)


# DEBUG mode

DEBUG = os.getenv("DEBUG_MODE", False)


# SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = os.getenv("SECRET_KEY")


# SECURITY WARNING: set this to your domain name in production!

# TODO: Change to environment settings
ALLOWED_HOSTS = ["*"]


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv("POSTGRES_DB", "postgres"),
        "USER": os.getenv("POSTGRES_USER", "postgres"),
        "HOST": os.getenv("POSTGRES_HOST", "db"),
        "PORT": int(os.getenv("POSTGRES_PORT", "5432")),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD", "postgres"),
    }
}


# Channels
# https://channels.readthedocs.io/en/latest/

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [
                (
                    os.getenv("REDIS_HOST", "redis"),
                    int(os.getenv("REDIS_PORT", "6379")),
                )
            ]
        },
    }
}


# Email

EMAIL_ENABLED = os.getenv("EMAIL_ENABLED", False)

if EMAIL_ENABLED:  # noqa: E501,F405
    EMAIL_HOST = os.getenv("EMAIL_HOST")  # noqa: F405
    EMAIL_PORT = int(os.getenv("EMAIL_PORT", 25))  # noqa: F405
    EMAIL_HOST_USER = os.getenv("EMAIL_HOST_USER")  # noqa: F405
    EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD")  # noqa: F405
    EMAIL_USE_TLS = bool(os.getenv("EMAIL_USE_TLS", False))  # noqa: F405
else:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
