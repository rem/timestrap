from .base import *  # noqa: F403


DEBUG = True


# SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = "CHANGE-ME"


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": os.environ.get("POSTGRES_HOST", "127.0.0.1"),
        "PORT": os.environ.get("POSTGRES_PORT", "5432"),
        "NAME": os.environ.get("POSTGRES_DB", "timestrap_dev"),
        "USER": os.environ.get("POSTGRES_USER", "timestrap_dev"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "secret"),
    }
}


# Channels
# https://channels.readthedocs.io/en/latest/

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [("localhost", 6379)]},
    }
}


# Email

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
