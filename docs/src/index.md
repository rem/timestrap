# Timestrap

[![Travis](https://img.shields.io/travis/overshard/timestrap.svg?style=for-the-badge)](https://travis-ci.org/overshard/timestrap) [![Coveralls](https://img.shields.io/coveralls/overshard/timestrap.svg?style=for-the-badge)](https://coveralls.io/github/overshard/timestrap) [![license](https://img.shields.io/github/license/overshard/timestrap.svg?style=for-the-badge)](https://github.com/overshard/timestrap/blob/master/LICENSE.md) [![Gitter](https://img.shields.io/gitter/room/nwjs/nw.js.svg?style=for-the-badge)](https://gitter.im/overshard/timestrap)

Time tracking you can host anywhere. Full export support in
multiple formats and easily extensible.


## Warning

This app is in development. It was forked from
[github.com:overshard/timestrap.git](https://github.com/overshard/timestrap).
Since then, the project was decoupled from an integrated containerized
environment. The goal of this project is to finalize the application and
ultimately release a version 1.0. The database model might change and migrations
can destroy your data. Ensure you backup your data, before upgrading.


## Documentation

More details and screenshots can be found on the main docs website:
[https://resing.dev/timestrap/](https://resing.dev/timestrap/)


## Demo

Of this project, there exists no demo instance. However, you can check out the
demo instance of the origin. It can be found on Heroku and resets every 10
minutes: [Demo instance](https://timestrap.herokuapp.com/)


## Superuser Credentials

On new setups, login with the default credentials:

- Username: `admin`
- Password: `admin`


## Docker/Podman Installation

A huge advantage of Podman is it's compatibility with Docker. Thus, you can
simply replace `docker` by `podman` for each command. It get's a bit more
complicated with `docker-compose`, but if you are a Podman user, I expect you to
be familiar enough to mirror the `docker-compose` setup.

This section will create a minimal docker server setup for Timestrap. A
containerized environment based on Docker or Podman. The initial setup will not
guarantee persistent data. In my personal setup, I am running my own Postgres
instance, which ensures data persistency. Use the search engine of your choice
to figure out how to use Docker volumes.


### Development Setup Requirements

- Docker / Podman
- Docker Compose

Docker Compose is solely used to simplify the development setup. A production
setup will be described as soon as the project ages. The `docker-compose.yml`
will start a PostgreSQL instance, a Redis instance and of course, the Timestrap
instance.


### Docker Running

On launch, each container will read a `.env` file with configuration parameters.
Make sure this file exists, otherwise Timestrap will read default values, which
might break the setup.

The following code snippet is filled with default values:

```
# ########################
# .env with default values
# ########################

# Django setup
DJANGO_SETTINGS_MODULE=timestrap.settings.docker
DJANGO_PORT=8000
SECRET_KEY=changeme

# Postgres setup
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
POSTGRES_USER=timestrap
POSTGRES_PASSWORD=timestrap
POSTGRES_DB=timestrap

# Redis setup
REDIS_HOST=127.0.0.1
REDIS_PORT=6379
```

To launch the Timestrap instance (and dependencies), run

```
docker-compose up --detach
```

If you do not require a dockerized PostgreSQL and Redis instance, then just
launch the Timestrap instance without dependent containers:

```
docker-compose up --detach timestrap
```

To migrate the database, create your first superuser, and create the initial
site configuration you then need to run:

```
sudo docker-compose exec web \
  python3 manage.py migrate  \
  --settings=timestrap.settings.docker
```

The timestrap application should now be running on the configured port of any
system you have exectued these commands on. On a local developer instance, open
[http://localhost:8000](http://localhost:8000) in your browser.


### Docker Data

All data should be stored in the timestrapdb volume. If you wish to rebuild
Timestrap at the latest you can do the following from the timestrap repo you
cloned:

```
git pull
sudo docker-compose stop
sudo docker-compose build
sudo docker-compose up --detach
sudo docker-compose exec web \
  python3 manage.py migrate  \
  --settings=timestrap.settings.docker
```

All data will be kept during this process and you'll have the latest version
of Timestrap.


## Development Installation

Contributions are appreciated. To contribute, simply fork the project and setup
a development instance as follows.


### Development Requirements

- Python 3.9+
- Python Dev
- poetry
- Node 12+
- npm
- Firefox
- geckodriver

Python 3.9+ is recommended since it's the most recent Python version when the
project was forked. Be warned, the code base is just tested on Python 3.9,
altough it might work with lower versions as well. The minimum requirement
is 3.5+, since it uses Channels to support WebSockets for realtime updates
on the client side.

If you are on Ubuntu, ensure you have installed Python Dev:

```
sudo apt install python3-dev
```


Unlike the original project does this fork use `poetry` instead of `pipenv`.
Install it as follows:

```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```


Similarly, Node12+ is just a recommendation. The initial project was working
with Node 8. The fork was just tested with Node 12. Older versions can work as
well but once again: Not tested.

Node installs npm by default but you may want to install the latest with
`npm install --global npm`.


Firefox is used for functional/selenium tests in conjunction with geckodriver,
you can get geckodriver from [mozilla's offical releases](https://github.com/mozilla/geckodriver/releases)
or you might be able to install it with your systems package manager. Brew on
macOS has this with `brew install geckodriver`. If you have to download it
manually make sure to extract it in some sort of `bin` directory e.g.
`/usr/local/bin/`.


### Development Setup

Once you have all of the above you can get started! For the global npm install
on gulp-cli you may need to run this with sudo depending on how you installed
everything above.

```
npm install --global gulp-cli
npm install
poetry install
```

After all the dependencies install you can migrate the database and run the
server.

```
gulp manage:migrate
gulp
```

If you'd like to have some sample data to work with you can run
`gulp manage:fake` after you run `gulp manage:migrate`.

Timestrap should now be running at [http://localhost:8000](http://localhost:8000)
and gulp + django's test server will automatically recognize and recompile
changes to any file allowing for quick modification and review.

Once you've made your changes you can test with `gulp coverage:development` and
if that is successful and you want to share your changes create a
[pull request](https://github.com/overshard/timestrap/pulls)!


### Development Commands

I've prebuilt a variety of build commands for development, you can see a list
of them by running `gulp --tasks` and I will breifly cover some of them here.

- `gulp` Will run a webserver with django and build the client with webpack
- `gulp lint` Will check all code for style consistency
- `gulp manage:makemigrations` Will generate new migrations if models changes
- `gulp manage:migrate` Makes sure there is a superuser, runs migrations
- `gulp manage:fake` Adds a bunch of fake data for testing
- `gulp manage:reset` Resets the database and adds fake data with a superuser
- `gulp coverage:development` Lints, runs tests, shows coverage report

