# ------------------------------------------------------------------
# Stage 0: Build frontend
# ------------------------------------------------------------------
FROM alpine as build-stage

WORKDIR /srv/timestrap

RUN apk add --no-cache nodejs nodejs-dev npm yarn

COPY . .

RUN npm install
RUN npm rebuild node-sass

# Writes to ./client/static
RUN yarn run build:prod


# ------------------------------------------------------------------
# Stage 1: Build & run backend
# ------------------------------------------------------------------
FROM python:3.9-alpine as run-stage

WORKDIR /srv/timestrap

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV POETRY_VERSION=1.1.4
ENV POETRY_HOME=/opt/poetry

# Install dependencies
#RUN apk add --no-cache nodejs nodejs-dev yarn \
#    postgresql-dev gcc musl-dev libffi-dev curl
RUN apk add --no-cache postgresql-dev gcc musl-dev libffi-dev curl

# Install poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - && \
  cd /usr/local/bin && \
  ln -s /opt/poetry/bin/poetry

# Configure poetry
RUN poetry config virtualenvs.create false

COPY ./pyproject.toml ./poetry.lock* package.json yarn.lock /srv/timestrap/

ARG INSTALL_DEV=false
RUN sh -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"

COPY . .

COPY --from=build-stage /srv/timestrap/client /srv/timestrap/client

#RUN yarn run build:prod
RUN python3 manage.py collectstatic --noinput
